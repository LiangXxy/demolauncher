package app.liangxy.com.demolauncher.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

import app.liangxy.com.demolauncher.R;
import app.liangxy.com.demolauncher.adapter.Demo2Adapter;
import app.liangxy.com.demolauncher.adapter.WorkspaceDemoAdapter;
import app.liangxy.com.demolauncher.application.MyApplication;
import app.liangxy.com.demolauncher.bean.ModelViewBean;
import app.liangxy.com.demolauncher.gen.AppInfo;
import app.liangxy.com.demolauncher.widget.DensityTool;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class MainFragment extends Fragment{
    private RecyclerView rv_main;
    private Activity activity;
    private PackageManager mPackageManager;
    private List<ResolveInfo> mApps;
    private int pageNumber;

    public static Fragment newInstance(int pageNumber){

        Fragment fragment = new  MainFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("pageNumber",pageNumber);

        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;

        pageNumber = getArguments().getInt("pageNumber");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main,container,false);
        init(root);
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init(View root){
        rv_main = root.findViewById(R.id.rv_main);
        //rv_main.setBackground(getResources().getDrawable(R.color.grey21));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false);

        rv_main.setLayoutManager(linearLayoutManager);

        mPackageManager = activity.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN,null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        mApps = mPackageManager.queryIntentActivities(intent,0);


//        List<ResolveInfo> nullResolveInfos = new ArrayList<>();
//        nullResolveInfos.add(null);
//        nullResolveInfos.add(null);
//        nullResolveInfos.add(null);
//        nullResolveInfos.add(null);
//
//        List<ModelViewBean> modelViewBeans = new ArrayList<>();
//
//        for(int i = 0;i<3;i++){
//            ModelViewBean modelViewBean = new ModelViewBean();
//            modelViewBean.type = "appInfo";
//            modelViewBean.resolveInfos = nullResolveInfos;
//            modelViewBeans.add(modelViewBean);
//        }
//        int temp = 0;
//
//        for(int i = 0;i<3;i++){
//            List<ResolveInfo> resolveInfos = new ArrayList<>();
//
//            for(int j = 0;j<4;j++){
//                if(temp<mApps.size()){
//                    resolveInfos.add(mApps.get(temp));
//                    temp++;
//                }else{
//                    resolveInfos.add(null);
//                }
//            }
//
//            ModelViewBean modelViewBean = new ModelViewBean();
//            modelViewBean.type = "appInfo";
//            modelViewBean.resolveInfos = resolveInfos;
//            modelViewBeans.add(modelViewBean);
//        }



        //Demo2Adapter adapter = new Demo2Adapter(activity,modelViewBeans);

        //rv_main.setAdapter(adapter);

        int viewWidth = DensityTool.getWidthPx(activity)/4;
        int height = DensityTool.getHeightPx(activity);

        int scale = (int) Math.floor(height/viewWidth);

        int startLine = pageNumber*(scale-1);
        List<ModelViewBean> modelViewBeans = new ArrayList<>();
        List<AppInfo> appInfos = MyApplication.getDaoSession().getAppInfoDao().queryBuilder().list();

        for(int i = startLine;i < startLine + 5;i++){
            List<ResolveInfo> resolveInfos = new ArrayList<>();
            for(int j = 0 ; j < 4; j++){
                AppInfo info = getAppInfo(appInfos,i,j);
                if(info != null){
                    resolveInfos.add(getResolveInfo(info,mApps));

                }else resolveInfos.add(null);
            }
            ModelViewBean modelViewBean = new ModelViewBean();
            modelViewBean.type = "appInfo";
            modelViewBean.resolveInfos = resolveInfos;
            modelViewBeans.add(modelViewBean);
        }

        Demo2Adapter adapter = new Demo2Adapter(activity,modelViewBeans);
        rv_main.setAdapter(adapter);

//        rv_main.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
//            @Override
//            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//                switch (e.getAction()) {
//                    case MotionEvent.ACTION_DOWN:{
//
//                        rv_main.requestDisallowInterceptTouchEvent(true);
//                        break;
//                    }
//                }
//                return false;
//            }
//
//            @Override
//            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//            }
//
//            @Override
//            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//            }
//        });
    }

    private AppInfo getAppInfo(List<AppInfo> appInfos,int line,int rank){
        if(appInfos.size()>0){
            for( AppInfo info : appInfos){
                if(info.getLine() == line && info.getRank() == rank){
                    return info;
                }
            }
        }
        return null;
    }

    private ResolveInfo getResolveInfo(AppInfo appInfo,List<ResolveInfo> list){
        for(ResolveInfo info : list){
            if(info.activityInfo.packageName.equals(appInfo.packageName)) return info;
        }
        return null;
    }

}
