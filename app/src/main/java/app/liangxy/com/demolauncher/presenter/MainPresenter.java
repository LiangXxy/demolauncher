package app.liangxy.com.demolauncher.presenter;

import android.content.Context;

import app.liangxy.com.demolauncher.model.MainModel;
import app.liangxy.com.demolauncher.view.MainView;

/**
 * Created by Administrator on 2021/1/26 0026.
 */

public class MainPresenter {
    private MainView view;
    private MainModel model;
    private Context context;

    public MainPresenter(MainView view, Context context){
        this.view = view;
        this.context = context;
    }

}
