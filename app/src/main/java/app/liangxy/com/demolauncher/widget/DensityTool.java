package app.liangxy.com.demolauncher.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Outline;
import android.os.Build;
import android.view.View;
import android.view.ViewOutlineProvider;

/**
 *
 * Created by Administrator on 2020/12/8 0008.
 */

public class DensityTool {

    //根据手机的分辨率从 dp 的单位 转成为 px(像素)
    public static float dp2px(Resources resources, float dpValue) {
        final float scale = resources.getDisplayMetrics().density;
        return (dpValue * scale + 0.5f);
    }
    //根据手机的分辨率从 dp 的单位 转成为 px(像素)
    public static int dp2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dpValue * scale + 0.5f);
    }
    //根据手机的分辨率从 px(像素) 的单位 转成为 dp
    public static float px2dp(Resources resources, float pxValue) {

        final float scale = resources.getDisplayMetrics().density;
        return (pxValue / scale + 0.5f);
    }
    //根据手机的分辨率从 sp(像素) 的单位 转成为 px
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }
    //根据手机的分辨率从 px(像素) 的单位 转成为 sp
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }
    //获取屏幕dpi
    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }
    //获取屏幕dpi
    public static int getDpi(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }
    //获取屏幕宽度px
    public static int getWidthPx(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }
    //获取屏幕高度px
    public static int getHeightPx(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setClipViewCornerRadius(View view, final int radius){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            //不支持5.0版本以下的系统
            return;
        }
        if(view == null) return;

        if(radius <= 0){
            return;
        }

        view.setOutlineProvider(new ViewOutlineProvider() {

            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0, 0, view.getWidth(),   view.getHeight(), radius);
            }
        });
        view.setClipToOutline(true);
    }

    /**
     //透明状态栏
     getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
     //透明导航栏
     getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

     * 获取状态栏高度
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 获取导航栏高度
     * @param context
     * @return
     */
    public static int getDaoHangHeight(Context context) {
        int result = 0;
        int resourceId=0;
        int rid = context.getResources().getIdentifier("config_showNavigationBar", "bool", "android");
        if (rid!=0){
            resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
//            CMLog.show("高度："+resourceId);
//            CMLog.show("高度："+context.getResources().getDimensionPixelSize(resourceId) +"");
            return context.getResources().getDimensionPixelSize(resourceId);
        }else
            return 0;
    }

}
