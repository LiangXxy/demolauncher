package app.liangxy.com.demolauncher.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import java.util.List;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class WorkspaceDemoAdapter extends MyFragmentPagerAdapter {
    private List<Fragment> list;
    public WorkspaceDemoAdapter(FragmentManager fm, List<Fragment> list){
        super(fm);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Fragment getItem(int var1) {
        return list.get(var1);
    }
}
