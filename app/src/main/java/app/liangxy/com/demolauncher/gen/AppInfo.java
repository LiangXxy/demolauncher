package app.liangxy.com.demolauncher.gen;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Administrator on 2021/1/27 0027.
 */
@Entity
public class AppInfo {
    @Id(autoincrement = true)
    public Long id;
    @Property
    public String name;
    @Property
    public String packageName;
    @Property
    public int line;
    @Property
    public int rank;
    @Generated(hash = 856082649)
    public AppInfo(Long id, String name, String packageName, int line, int rank) {
        this.id = id;
        this.name = name;
        this.packageName = packageName;
        this.line = line;
        this.rank = rank;
    }
    @Generated(hash = 1656151854)
    public AppInfo() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPackageName() {
        return this.packageName;
    }
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public int getLine() {
        return this.line;
    }
    public void setLine(int line) {
        this.line = line;
    }
    public int getRank() {
        return this.rank;
    }
    public void setRank(int rank) {
        this.rank = rank;
    }
}
