package app.liangxy.com.demolauncher.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import app.liangxy.com.demolauncher.CustomView.DemoViewPager;
import app.liangxy.com.demolauncher.CustomView.MyCirclePageIndicator;
import app.liangxy.com.demolauncher.CustomView.Workspace;
import app.liangxy.com.demolauncher.R;
import app.liangxy.com.demolauncher.adapter.MyFragmentPagerAdapter;
import app.liangxy.com.demolauncher.adapter.MyViewPager;
import app.liangxy.com.demolauncher.adapter.WorkspaceDemoAdapter;
import app.liangxy.com.demolauncher.application.MyApplication;
import app.liangxy.com.demolauncher.bean.ModelViewBean;
import app.liangxy.com.demolauncher.fragment.MainFragment;
import app.liangxy.com.demolauncher.gen.AppInfo;
import app.liangxy.com.demolauncher.widget.DensityTool;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class Demo1Activity extends Activity {
    private DemoViewPager ws_demo;
    private LinearLayout ll_demo;
    private int scale;
    private MyCirclePageIndicator indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo1);
        init();
    }

    private void init(){

        Intent intent = new Intent(Intent.ACTION_MAIN,null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> mApps = this.getPackageManager().queryIntentActivities(intent,0);

        int viewWidth = DensityTool.getWidthPx(this)/4;
        int height = DensityTool.getHeightPx(this);

        scale = (int) Math.floor(height/viewWidth);


        if(MyApplication.getDaoSession().getAppInfoDao().count()<=0){
            Log.w("LiangXy","MyApplication.getDaoSession().getAppInfoDao().count()<=0");
            int startLine = scale-2-1;
            int startRank = 0;


            Log.w("LiangXy","viewWidth"+viewWidth+"height"+height+"scale:"+scale);

            for(ResolveInfo info : mApps){

                AppInfo appInfo = new AppInfo(null,info.loadLabel(getPackageManager()).toString(),info.activityInfo.packageName
                        ,startLine,startRank);
                MyApplication.getDaoSession().getAppInfoDao().insert(appInfo);

                if(startRank == 3) {startLine++;startRank = 0;}
                else startRank++;
            }

        }else if(mApps.size() != MyApplication.getDaoSession().getAppInfoDao().count()){

            Log.w("LiangXy","mApps.size() != MyApplication.getDaoSession().getAppInfoDao().count()");

            List<AppInfo> list = MyApplication.getDaoSession().getAppInfoDao().queryBuilder().list();

            for(ResolveInfo info : mApps){
                if(!ifHaveApp(list,info)){
                    int line = list.get(list.size()-1).line;
                    int rank = list.get(list.size()-1).rank;
                    AppInfo appInfo = new AppInfo(null,info.loadLabel(getPackageManager()).toString(),info.activityInfo.packageName
                            ,rank == 3?line+1:line,rank == 3?0:rank+1);
                    list.add(appInfo);
                    MyApplication.getDaoSession().getAppInfoDao().insert(appInfo);
                }
            }
        }
        Log.w("LiangXy","mApps.size():"+mApps.size()+"getAppInfoDao().count():"
                +MyApplication.getDaoSession().getAppInfoDao().count());

//        int viewWidth = DensityTool.getWidthPx(this)/4;
//        int height = DensityTool.getHeightPx(this);
//
//        int scale = (int) Math.floor(height/viewWidth);
//
//
//
//        Log.w("LiangXy","viewWidth"+viewWidth+"height"+height+"scale:"+scale);

        findView();
        viewInit();
    }

    private void findView(){
        indicator = findViewById(R.id.indicator);
        ws_demo = findViewById(R.id.ws_demo);
        ll_demo = findViewById(R.id.ll_demo);
    }
    private void viewInit(){

        List<Fragment> fragmentList = new ArrayList<>();

        List<AppInfo> appInfos = MyApplication.getDaoSession().getAppInfoDao().queryBuilder().list();
        double pagerNumber =  Math.ceil(((double) appInfos.get(appInfos.size()-1).getLine()+1)/(scale-1));
        Log.w("LiangXy","appInfos.get(appInfos.size()-1).getLine()+1:"+(appInfos.get(appInfos.size()-1).getLine()+1));
        Log.w("LiangXy","scale-1:"+(scale-1));
        Log.w("LiangXy","pagerNumber:"+pagerNumber);

        for(int i = 0;i<pagerNumber;i++){
            fragmentList.add(MainFragment.newInstance(i));
        }

        WorkspaceDemoAdapter adapter = new WorkspaceDemoAdapter(getFragmentManager(),fragmentList);

//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
//                (LinearLayout.LayoutParams.MATCH_PARENT, DensityTool.getHeightPx(this)-);


//        List<AppInfo> appInfos = MyApplication.getDaoSession().getAppInfoDao().queryBuilder().list();
//
//        for(AppInfo info : appInfos){
//            Log.w("LiangXy",info.getPackageName()+":"+info.getLine()+":"+info.getRank());
//        }

        ws_demo.setAdapter(adapter);
        ws_demo.setOverScrollMode( View.OVER_SCROLL_NEVER);

        //ws_demo.setBackground(getResources().getDrawable(R.color.white));

        ll_demo.getLayoutParams().height = DensityTool.getWidthPx(this)/4;
        //ll_demo.setBackground(getResources().getDrawable(R.color.red));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.touming));
        }

        //DisableLRScroll(ws_demo);


        indicator.setViewPager(ws_demo);
        indicator.setSnap(true);
        indicator.onPageSelected(0);


    }


    private boolean ifHaveApp(List<AppInfo> appInfos ,ResolveInfo resolveInfo){

        for(AppInfo info : appInfos){
            if(info.packageName.equals(resolveInfo.activityInfo.packageName))return true;
        }
        return false;
    }

}
