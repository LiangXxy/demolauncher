package app.liangxy.com.demolauncher.bean;

import android.content.pm.ResolveInfo;

import java.util.List;

import app.liangxy.com.demolauncher.gen.AppInfo;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class ModelViewBean {
    public String type;

    public TimeModel timeModel;

    public List<ResolveInfo> resolveInfos;

    public class TimeModel{
        public String time;
        public String place;
        public int weekDay;
        public int month;
        public int monthDay;
        public int averageTemp;
        public int maxTemp;
        public int minTemp;
    }
}
