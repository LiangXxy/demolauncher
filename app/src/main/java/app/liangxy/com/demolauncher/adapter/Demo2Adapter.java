package app.liangxy.com.demolauncher.adapter;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.liangxy.com.demolauncher.bean.ModelViewBean;
import app.liangxy.com.demolauncher.gen.AppInfo;
import app.liangxy.com.demolauncher.widget.DensityTool;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class Demo2Adapter extends RecyclerView.Adapter<Demo2Adapter.ModelViewHolder>{
    private List<ModelViewBean>list;
    private Context context;
    private RecyclerView item_recyclerview;


    public Demo2Adapter(Context context, List<ModelViewBean> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView recyclerView = new RecyclerView(context);


        return new ModelViewHolder(recyclerView);
    }

    @Override
    public void onBindViewHolder(ModelViewHolder holder, int position) {

        if(list.get(position).type.equals("appInfo")){
            GridLayoutManager manager = new GridLayoutManager(context,4);
            holder.recyclerView.setLayoutManager(manager);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,DensityTool.getWidthPx(context)/4);

            holder.recyclerView.setLayoutParams(layoutParams);

            DemoAdapter demoAdapter = new DemoAdapter(list.get(position).resolveInfos,context);

            holder.recyclerView.setAdapter(demoAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ModelViewHolder extends RecyclerView.ViewHolder{

        public RecyclerView recyclerView;

        public ModelViewHolder(View itemView) {
            super(itemView);
            recyclerView = (RecyclerView) itemView;
        }
    }


}
