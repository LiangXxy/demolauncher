package app.liangxy.com.demolauncher.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.liangxy.com.demolauncher.R;
import app.liangxy.com.demolauncher.widget.DensityTool;

/**
 * Created by Administrator on 2021/1/26 0026.
 */

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.MyViewHolder>{
    private List<ResolveInfo>list;
    private Context context;

    public DemoAdapter(List<ResolveInfo> list , Context context){
        this.list = list;
        this.context = context;
    }

    @Override
    public DemoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int windowWidth = DensityTool.getWidthPx(context);
        int temp = DensityTool.dp2px(context,1);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (windowWidth/4,windowWidth/4);
        layoutParams.setMargins(temp*5,temp*15,temp*5,temp*15);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setLayoutParams(layoutParams);

        ImageView imageView = new ImageView(context);
        imageView.setTag("image");
        TextView textView = new TextView(context);
        textView.setTag("text");
        textView.setMaxLines(1);
        textView.setTextSize(12);
        textView.setTextColor(context.getResources().getColor(R.color.grey21));
        textView.setGravity(Gravity.CENTER);
        //textView.setEllipsize(TextUtils.TruncateAt.END);

//        textView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:{
//                        Log.w("LiangXy","MotionEvent.ACTION_DOWN");
//                        break;
//                    }
//                    case MotionEvent.ACTION_MOVE:{
//                        Log.w("LiangXy","MotionEvent.ACTION_MOVE");
//                        break;
//                    }
//                    case MotionEvent.ACTION_UP:{
//                        Log.w("LiangXy","MotionEvent.ACTION_UP");
//                        break;
//                    }
//                }
//                return false;
//            }
//        });


        linearLayout.addView(imageView,windowWidth/7,windowWidth/7);
        linearLayout.addView(textView,windowWidth/4,LinearLayout.LayoutParams.WRAP_CONTENT);

        return new MyViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if(list.get(position)!=null){
            holder.textView.setText(list.get(position).loadLabel(context.getPackageManager()));
            holder.imageView.setImageDrawable(list.get(position).loadIcon(context.getPackageManager()));
        }




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =context.getPackageManager().getLaunchIntentForPackage(list.get(position).activityInfo.packageName);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewWithTag("text");
            imageView = itemView.findViewWithTag("image");
        }
    }

}
