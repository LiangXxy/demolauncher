package app.liangxy.com.demolauncher.application;

import android.app.Application;

import app.liangxy.com.demolauncher.gen.DaoMaster;
import app.liangxy.com.demolauncher.gen.DaoSession;

/**
 * Created by Administrator on 2021/1/27 0027.
 */

public class MyApplication extends Application{
    public static DaoSession daoSession;
    @Override
    public void onCreate() {
        super.onCreate();
        greenInit();
    }

    private void greenInit(){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,"Demo.db",null);

        DaoMaster daoMaster = new DaoMaster(helper.getEncryptedWritableDb("LiangXy"));

        daoSession = daoMaster.newSession();
        daoSession.clear();
    }

    public static DaoSession getDaoSession(){
        return daoSession;
    }
}
