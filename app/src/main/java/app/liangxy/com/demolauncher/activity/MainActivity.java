package app.liangxy.com.demolauncher.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.util.List;
import java.util.ResourceBundle;

import app.liangxy.com.demolauncher.R;
import app.liangxy.com.demolauncher.adapter.DemoAdapter;
import app.liangxy.com.demolauncher.presenter.MainPresenter;
import app.liangxy.com.demolauncher.view.MainView;

public class MainActivity extends Activity implements MainView{
    private MainPresenter presenter;
    private PackageManager mPackageManager;
    private List<ResolveInfo> mApps;

    private RecyclerView rv_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){
        presenter = new MainPresenter(this,this);

        mPackageManager = this.getPackageManager();
        Intent  intent = new Intent(Intent.ACTION_MAIN,null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        mApps = mPackageManager.queryIntentActivities(intent,0);

        for(ResolveInfo resolveInfo:mApps){
            Log.w("LiangXy",""+resolveInfo.loadLabel(mPackageManager));
        }
        findView();
        viewInit();
    }

    private void findView(){
        rv_main = findViewById(R.id.rv_main);
    }

    private void viewInit(){
        DemoAdapter adapter = new DemoAdapter(mApps,this);
        GridLayoutManager layoutManager = new GridLayoutManager(this,4);

        rv_main.setLayoutManager(layoutManager);
        rv_main.setAdapter(adapter);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.touming));
        }
//        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            //使用SystemBarTint库使4.4版本状态栏变色，需要先将状态栏设置为透明
//            transparencyBar(activity);
//            SystemBarTintManager tintManager = new SystemBarTintManager(activity);
//            tintManager.setStatusBarTintEnabled(true);
//            tintManager.setStatusBarTintResource(colorId);
//        }

    }




}
